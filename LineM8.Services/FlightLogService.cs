﻿using LineM8.Data;
using LineM8.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace LineM8.Services
{
    public class FlightLogService : IFlightLog
    {
        private readonly LineM8DbContext _context;

        public FlightLogService(LineM8DbContext context)
        {
            _context = context;
        }
        public IEnumerable<FlightLog> GetAll()
        {
            return _context.FlightLogs;
        }

        public FlightLog GetById(int id)
        {
            return _context.FlightLogs.Find(id);
        }

        public IEnumerable<FlightLog> GetByAirportId()
        {
            return GetAll().Where()
        }

        public IEnumerable<FlightLog> GetByArrivalDate()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FlightLog> GetByArrivalFlightNumber()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<FlightLog> GetByAirlineMaintenanceCheckId()
        {
            throw new NotImplementedException();
        }






    }
}
