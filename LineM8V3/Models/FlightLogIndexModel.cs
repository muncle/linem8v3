﻿using LineM8.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LineM8V3.Models
{
    public class FlightLogIndexModel
    {
        [Key]
        public int FlightLogId { get; set; }
        public int AirportId { get; set; }
        public int AirlineId { get; set; }
        public int AircraftTypeId { get; set; }
        public int AircraftIdentityId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Arrival Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ArrivalDate { get; set; }

        [Required]
        [Display(Name = "Arrival Flight Number")]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        [StringLength(10)]
        public string ArrivalFlightNumber { get; set; }

        [Required]
        [Display(Name = "Arrival Time")]
        [DataType(DataType.Time)]
        public TimeSpan ActualArrivalTime { get; set; }

        [Required]
        [Display(Name = "Arrival Parking Bay")]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        [StringLength(10)]
        public string ArrivalParkingBay { get; set; }

        [Required]
        [Display(Name = "Arriving From")]
        [RegularExpression(@"^[A-Z]*$")]
        [StringLength(3, MinimumLength = 3)]
        public string ArrivingFromAirport { get; set; }

        //public int ArrivalStaff { get; set; }

        public int AirlineMaintenanceCheckId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Departure Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DepartureDate { get; set; }

        [Required]
        [Display(Name = "Departure Flight Number")]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        [StringLength(10)]
        public string DepartureFlightNumber { get; set; }

        [Required]
        [Display(Name = "Departure Time")]
        [DataType(DataType.Time)]
        public TimeSpan ActualDepartureTime { get; set; }

        [Required]
        [Display(Name = "Departure Parking Bay")]
        [RegularExpression(@"^[A-Z0-9-]*$")]
        [StringLength(10)]
        public string DepartureParkingBay { get; set; }

        [Required]
        [Display(Name = "Departing To")]
        [RegularExpression(@"^[A-Z]*$")]
        [StringLength(3, MinimumLength = 3)]
        public string DepartingToAirport { get; set; }

        //public int DepartureStaff { get; set; }

        [Display(Name = "Remarks")]
        public string FlightRemarks { get; set; }

        [Timestamp]
        public DateTime RecordCreated { get; set; }

        [Timestamp]
        public DateTime RecordUpdated { get; set; }

        //public int FlightLogInputUser { get; set; }


        //public LineStation LineStation { get; set; }
        public Airport Airport { get; set; }
        public Airline Airline { get; set; }
        //public AircraftType AircraftType { get; set; }
        public AircraftIdentity AircraftIdentity { get; set; }
        public AirlineMaintenanceCheck AirlineMaintenanceCheck { get; set; }
    }
}
