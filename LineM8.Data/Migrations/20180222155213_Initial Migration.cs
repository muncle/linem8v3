﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace LineM8.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AircraftIdentities",
                columns: table => new
                {
                    AircraftIdentityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AircraftIdentifier = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AircraftIdentities", x => x.AircraftIdentityId);
                });

            migrationBuilder.CreateTable(
                name: "AirlineMaintenanceChecks",
                columns: table => new
                {
                    AirlineMaintenanceCheckId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AirlineMaintenanceCheckCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AirlineMaintenanceChecks", x => x.AirlineMaintenanceCheckId);
                });

            migrationBuilder.CreateTable(
                name: "Airlines",
                columns: table => new
                {
                    AirlineId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AirlineIATACode = table.Column<string>(nullable: true),
                    AirlineICAOCode = table.Column<string>(nullable: true),
                    AirlineName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airlines", x => x.AirlineId);
                });

            migrationBuilder.CreateTable(
                name: "Airports",
                columns: table => new
                {
                    AirportId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AirportIATACode = table.Column<string>(nullable: true),
                    AirportName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Airports", x => x.AirportId);
                });

            migrationBuilder.CreateTable(
                name: "FlightLogs",
                columns: table => new
                {
                    FlightLogId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActualArrivalTime = table.Column<TimeSpan>(nullable: false),
                    ActualDepartureTime = table.Column<TimeSpan>(nullable: false),
                    AircraftIdentityId = table.Column<int>(nullable: false),
                    AirlineId = table.Column<int>(nullable: false),
                    AirlineMaintenanceCheckId = table.Column<int>(nullable: false),
                    AirportId = table.Column<int>(nullable: false),
                    ArrivalDate = table.Column<DateTime>(nullable: false),
                    ArrivalFlightNumber = table.Column<string>(nullable: true),
                    ArrivalParkingBay = table.Column<string>(nullable: true),
                    ArrivingFromAirport = table.Column<string>(nullable: true),
                    DepartingToAirport = table.Column<string>(nullable: true),
                    DepartureDate = table.Column<DateTime>(nullable: false),
                    DepartureFlightNumber = table.Column<string>(nullable: true),
                    DepartureParkingBay = table.Column<string>(nullable: true),
                    FlightRemarks = table.Column<string>(nullable: true),
                    RecordCreated = table.Column<DateTime>(nullable: false),
                    RecordUpdated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlightLogs", x => x.FlightLogId);
                    table.ForeignKey(
                        name: "FK_FlightLogs_AircraftIdentities_AircraftIdentityId",
                        column: x => x.AircraftIdentityId,
                        principalTable: "AircraftIdentities",
                        principalColumn: "AircraftIdentityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlightLogs_Airlines_AirlineId",
                        column: x => x.AirlineId,
                        principalTable: "Airlines",
                        principalColumn: "AirlineId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlightLogs_AirlineMaintenanceChecks_AirlineMaintenanceCheckId",
                        column: x => x.AirlineMaintenanceCheckId,
                        principalTable: "AirlineMaintenanceChecks",
                        principalColumn: "AirlineMaintenanceCheckId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlightLogs_Airports_AirportId",
                        column: x => x.AirportId,
                        principalTable: "Airports",
                        principalColumn: "AirportId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AircraftIdentityId",
                table: "FlightLogs",
                column: "AircraftIdentityId");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AirlineId",
                table: "FlightLogs",
                column: "AirlineId");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AirlineMaintenanceCheckId",
                table: "FlightLogs",
                column: "AirlineMaintenanceCheckId");

            migrationBuilder.CreateIndex(
                name: "IX_FlightLogs_AirportId",
                table: "FlightLogs",
                column: "AirportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FlightLogs");

            migrationBuilder.DropTable(
                name: "AircraftIdentities");

            migrationBuilder.DropTable(
                name: "Airlines");

            migrationBuilder.DropTable(
                name: "AirlineMaintenanceChecks");

            migrationBuilder.DropTable(
                name: "Airports");
        }
    }
}
