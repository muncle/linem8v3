﻿using LineM8.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8.Data
{
    public interface IFlightLog
    {
        IEnumerable<FlightLog> GetAll();
        FlightLog GetById(int id);
        IEnumerable<FlightLog> GetByArrivalDate();
        IEnumerable<FlightLog> GetByArrivalFlightNumber();
        IEnumerable<FlightLog> GetByAirportId();
        IEnumerable<FlightLog> GetByAirlineMaintenanceCheckId();
    }
}
