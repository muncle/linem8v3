﻿using LineM8.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace LineM8.Data
{
    public class LineM8DbContext : DbContext
    {
        public LineM8DbContext(DbContextOptions options) : base(options) { }

        public DbSet<FlightLog> FlightLogs { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<AircraftIdentity> AircraftIdentities { get; set; }
        public DbSet<AirlineMaintenanceCheck> AirlineMaintenanceChecks { get; set; }
    }
}
