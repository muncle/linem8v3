﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8.Data.Models
{
    public class Airport
    {
        public int AirportId { get; set; }
        public string AirportIATACode { get; set; }
        public string AirportName { get; set; }
    }
}
