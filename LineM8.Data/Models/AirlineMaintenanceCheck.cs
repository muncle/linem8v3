﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8.Data.Models
{
    public class AirlineMaintenanceCheck
    {
        public int AirlineMaintenanceCheckId { get; set; }
        public string AirlineMaintenanceCheckCode { get; set; }
    }
}
