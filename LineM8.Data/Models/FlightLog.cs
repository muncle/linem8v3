﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8.Data.Models
{
    public class FlightLog
    {
        public int FlightLogId { get; set; }
        public int AirportId { get; set; }
        //public IEnumerable<Airport> Airports { get; set; }
        public int AirlineId { get; set; }
        //public IEnumerable<Airline> Airlines { get; set; }
        public int AircraftIdentityId { get; set; }
        //public IEnumerable<AircraftIdentity> AircraftIdentity { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ArrivalFlightNumber { get; set; }
        public TimeSpan ActualArrivalTime { get; set; }
        public string ArrivalParkingBay { get; set; }
        public string ArrivingFromAirport { get; set; }
        public int AirlineMaintenanceCheckId { get; set; }
        //public IEnumerable<int> AirlineMaintenanceCheck { get; set; }
        public DateTime DepartureDate { get; set; }
        public string DepartureFlightNumber { get; set; }
        public TimeSpan ActualDepartureTime { get; set; }
        public string DepartureParkingBay { get; set; }
        public string DepartingToAirport { get; set; }
        public string FlightRemarks { get; set; }
        public DateTime RecordCreated { get; set; }
        public DateTime RecordUpdated { get; set; }
        //public int FlightLogInputUser { get; set; }
        
        //public LineStation LineStation { get; set; }
        public Airport Airport { get; set; }
        public Airline Airline { get; set; }
        //public AircraftType AircraftType { get; set; }
        public AircraftIdentity AircraftIdentity { get; set; }
        public AirlineMaintenanceCheck AirlineMaintenanceCheck { get; set; }
    }
}
