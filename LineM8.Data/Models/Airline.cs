﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8.Data.Models
{
    public class Airline
    {
        public int AirlineId { get; set; }
        public string AirlineName { get; set; }
        public string AirlineICAOCode { get; set; }
        public string AirlineIATACode { get; set; }
    }
}
