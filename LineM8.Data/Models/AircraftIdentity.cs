﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineM8.Data.Models
{
    public class AircraftIdentity
    {
        public int AircraftIdentityId { get; set; }
        public string AircraftIdentifier { get; set; }
    }
}
